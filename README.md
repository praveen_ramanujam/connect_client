### What is this repository for? ###

* This repository provides example codes of connecting to the Connect backend.
* Version 0.1

### How do I get set up? ###

Please request the following to Rapyuta Robotics

* Backend server address
* Authentication Token
* Login Credentials

### Who do I talk to? ###

* praveen.ramanujam@rapyuta-robotics.com
* dominique.hunziker@rapyuta-robotics.com